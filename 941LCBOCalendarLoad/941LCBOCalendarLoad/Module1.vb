﻿Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports System.Globalization
Imports Oracle.DataAccess.Client

'Imports System.Data.OleDb



Module Module1

    ''' <summary>
    ''' Purpose: The script inserts data in to the calendar table of ROCON which drives the
    ''' valid delivery dates that are provided to the Webstore customers to choose from  
    ''' Usage: ROCON_CalendarUpdate.exe startdate enddate
    ''' Date Format: YYYY-MM-DD, startdate and enddate are inclusive dates
    ''' If the dates are not provided then the startdate and enddate default to what is provided in the config file
    ''' Created By: Ria, November 2012
    ''' 
    ''' Update: Change to use  1) Oracle.DataAccess 
    '''                        2) LCBO.HOLIDAY_EXCLUSION Table for holiday days instead of DBO.LCBO_HOLIDAY table
    '''                        3) instead of writing directly to the DBO.LCBO_CALENDAR table, write to DBO.LCBO_CALENDAR_PREVIEW Table instead
    '''                        4) Added is_Holiday in PREVIEW table to indicate calendar lands on a Holiday
    '''                        
    ''' TO RUN the script - 1) configure App.config file with the start and end date to generate the calendar
    '''                     2) run sql to delete all records in PREVIEW Table (DELETE FROM DBO.LCBO_CALENDAR_PREVIEW)
    '''                     3) data will be in DBO.LCBO_CALENDAR_PREVIEW table - export results to excel and send to B2B (Kelly Maxwell) to validate
    '''                     4) Once validated, load excel columns excluding IS_HOLIDAY to main table - DBO.LCBO_CALENDAR
    '''                     
    ''' Updated By: Shirley, November 2020 - use Oracle.DataAccess Instead + Change to look at LCBO.HOLIDAY_EXCLUSION Table instead
    ''' </summary>
    ''' <remarks></remarks>

    Sub Main()

        Dim log_path As String = ""
        log_path = System.Configuration.ConfigurationManager.AppSettings("logpath")
        Dim objStreamWriter As StreamWriter = Nothing
        Try
            objStreamWriter = File.AppendText(log_path)

            'Read the start date and end date as the paramaters
            Dim args() As String = System.Environment.GetCommandLineArgs
            Dim start_date As String = ""
            Dim end_date As String = ""
            Dim date_format As String = "yyyy-mm-dd"
            Dim date_value As DateTime
            Dim valid_date As Boolean = False

            If args.Length = 2 Then
                start_date = args(0)
                end_date = args(1)
            ElseIf args.Length <> 2 Then
                start_date = System.Configuration.ConfigurationManager.AppSettings("sdate")
                end_date = System.Configuration.ConfigurationManager.AppSettings("edate")
            End If

            objStreamWriter.WriteLine("Started at: " & DateTime.Now & " ")

            If DateTime.TryParseExact(start_date, date_format, CultureInfo.CurrentCulture, DateTimeStyles.None, date_value) And _
            DateTime.TryParseExact(end_date, date_format, CultureInfo.CurrentCulture, DateTimeStyles.None, date_value) Then
                valid_date = True
            Else
                valid_date = False
                objStreamWriter.WriteLine("Date Format incorrect:YYYY-MM-DD " & start_date & " " & end_date & " ")
            End If

            If DateTime.Compare(start_date, end_date) <= 0 Then
                valid_date = True
            Else
                valid_date = False
                objStreamWriter.WriteLine("start_date is greater than end_date " & start_date & " " & end_date & " ")
            End If

            ' The start_date and end_date are in the correct format and end_date is greater than the start_date
            If valid_date = True Then
                Dim next_day As String = ""
                Dim next_date As String = ""
                Dim to_day As String = ""
                Dim to_date As String = ""
                Dim counter As Integer = 1
                Dim isHoliday As Integer = 0

                Dim startdate As DateTime = Convert.ToDateTime(start_date)
                Dim enddate As DateTime = Convert.ToDateTime(end_date)
                Dim nextdate As DateTime = Nothing

                'Run for the start date and end date inclusive
                'Check if the next day is a statutary holiday or a weekend day until you find a business day for delivery
                While startdate <= enddate
                    to_date = startdate.ToString("yyyy-MM-dd")
                    to_day = DateTime.Parse(startdate).DayOfWeek
                    next_date = startdate.AddDays(1).ToString("yyyy-MM-dd")
                    nextdate = Convert.ToDateTime(next_date)
                    next_day = DateTime.Parse(nextdate).DayOfWeek
                    counter = 1
                    isHoliday = 0


                    'If today is a business day, keep checking next day until the next day is not a holiday or Saturday or Sunday
                    If Not checkStatHoliday(to_date) Or to_day <> 0 Or to_day <> 6 Then
                        While checkStatHoliday(next_date) Or next_day = 0 Or next_day = 6 Or counter = 7
                            next_date = nextdate.AddDays(1).ToString("yyyy-MM-dd")
                            nextdate = Convert.ToDateTime(next_date)
                            next_day = DateTime.Parse(nextdate).DayOfWeek
                            counter = counter + 1
                        End While

                    End If

                    'If today is a holiday or Saturday or Sunday the keep looking for the delivery date as nextday+1 
                    If CheckStatHoliday(to_date) Or to_day = 0 Or to_day = 6 Then
                        While CheckStatHoliday(next_date) Or next_day = 0 Or next_day = 6 Or counter = 7
                            next_date = nextdate.AddDays(1).ToString("yyyy-MM-dd")
                            nextdate = Convert.ToDateTime(next_date)
                            next_day = DateTime.Parse(nextdate).DayOfWeek
                            counter = counter + 1
                        End While
                        'The next day provided its not a holiday is the delivery date, loop until the first working day
                        next_date = nextdate.AddDays(1).ToString("yyyy-MM-dd")
                        While CheckStatHoliday(next_date)
                            nextdate = Convert.ToDateTime(next_date)
                            next_date = nextdate.AddDays(1).ToString("yyyy-MM-dd")
                        End While
                    End If

                    objStreamWriter.WriteLine("cut_off: " & startdate.ToString("yyyy-MM-dd") & " 13:30:00 delivery_date: " & next_date & "")
                    InsertCalendarEntry(startdate.ToString("yyyy-MM-dd"), next_date, CheckStatHoliday(startdate.ToString("yyyy-MM-dd")))
                    startdate = startdate.AddDays(1)
                End While
                'New requirement that the holidays and the weekends cutoff should have the status flag =0
                UpdateHolidayEntry()
                UpdateWeekendEntry()
            End If

            objStreamWriter.WriteLine("Ended at: " & DateTime.Now & " ")
            objStreamWriter.Close()
        Catch ex As Exception
            objStreamWriter.WriteLine(ex.Message)
            objStreamWriter.WriteLine("Ended at: " & DateTime.Now & " ")
            objStreamWriter.Close()
        End Try
    End Sub

    'Function checkStatHoliday(ByVal check_date As String) As Boolean

    '    Dim conn As OracleConnection = getOraConnection()
    '    Dim cmd As OracleCommand = Nothing
    '    Dim rdr As OracleDataReader = Nothing
    '    Dim stmt As String = ""
    '    Dim count As Integer = 0
    '    Dim result As Boolean = False

    '    stmt = "SELECT count(*) from dbo.LCBO_HOLIDAY where to_char(HOLIDAY_DATE,'YYYY-MM-DD')='" & check_date & "'"
    '    Try
    '        conn.Open()
    '        cmd = New OracleCommand(stmt, conn)
    '        rdr = cmd.ExecuteReader

    '        While rdr.Read
    '            count = rdr(0)
    '        End While

    '    Catch ex As Exception
    '        cmd.Dispose()
    '        rdr.Close()
    '        conn.Close()
    '    End Try
    '    cmd.Dispose()
    '    rdr.Close()
    '    conn.Close()

    '    If count > 0 Then
    '        result = True
    '    Else
    '        result = False
    '    End If
    '    Return result
    'End Function

    'Function getOraConnection() As OracleConnection
    '    Dim conn As OracleConnection = Nothing
    '    Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")
    '    conn = New OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings(env).ToString)
    '    Return conn
    'End Function

    Function getConn() As OracleConnection
        Return New OracleConnection(Configuration.ConfigurationManager.ConnectionStrings(Configuration.ConfigurationManager.AppSettings("env")).ToString())
    End Function

    Function CheckStatHoliday(ByVal check_date As String) As Boolean

        Dim conn As OracleConnection = getConn()
        Dim cmd As OracleCommand = Nothing
        Dim rdr As OracleDataReader = Nothing
        Dim stmt As String = ""
        Dim count As Integer = 0
        Dim result As Boolean = False

        'stmt = "SELECT count(*) from dbo.LCBO_HOLIDAY where to_char(HOLIDAY_DATE,'YYYY-MM-DD')='" & check_date & "'"
        stmt = "SELECT count(1) from lcbo.holiday_exclusion where HOLIDAY_DT = '" & check_date & "'"

        Try
            conn.Open()
            cmd = New OracleCommand(stmt, conn)
            rdr = cmd.ExecuteReader

            While rdr.Read
                count = rdr(0)
            End While

        Catch ex As Exception
            cmd.Dispose()
            rdr.Close()
            conn.Close()
        End Try
        cmd.Dispose()
        rdr.Close()
        conn.Close()

        If count > 0 Then
            result = True
        Else
            result = False
        End If
        Return result
    End Function

    Sub InsertCalendarEntry(ByVal cut_off As String, ByVal delivery_date As String, ByVal isHoliday As Boolean)
        Dim conn As OracleConnection = getConn()
        Dim cmd As OracleCommand = Nothing

        Dim stmt As String = ""
        Dim timeval As String = ""

        timeval = System.Configuration.ConfigurationManager.AppSettings("timeval")

        ' stmt = "INSERT into dbo.LCBO_CALENDAR(cut_off_datetime, delivery_date, status) " & _
        ' "values(to_date('" & cut_off & " " & timeval & "','YYYY-MM-DD HH24:MI:SS'), to_date('" & delivery_date & "','YYYY-MM-DD'),1)"


        stmt = "INSERT into dbo.LCBO_CALENDAR_PREVIEW(cut_off_datetime, delivery_date, status, IS_Holiday) " &
               "values(to_date('" & cut_off & " " & timeval & "','YYYY-MM-DD HH24:MI:SS'), to_date('" & delivery_date & "','YYYY-MM-DD'),1," & IIf(isHoliday, 1, 0) & ")"
        Try
            conn.Open()
            cmd = New OracleCommand(stmt, conn)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            cmd.Dispose()
            conn.Close()
        End Try
        cmd.Dispose()
        conn.Close()

    End Sub
    'If the cut off day is a holiday then update the status=0
    Sub UpdateHolidayEntry()
        Dim conn As OracleConnection = getConn()
        Dim cmd As OracleCommand = Nothing

        Dim stmt As String = ""
        Dim timeval As String = ""

        timeval = System.Configuration.ConfigurationManager.AppSettings("timeval")

        'stmt = "Update dbo.LCBO_CALENDAR set status=0 where to_char(cut_off_datetime,'YYYY-MM-DD') in " &
        '        " (select distinct to_char(holiday_date,'YYYY-MM-DD') from dbo.LCBO_HOLIDAY)"

        stmt = "Update dbo.LCBO_CALENDAR_PREVIEW set status=0 where to_char(cut_off_datetime,'YYYY-MM-DD') in " &
                " (select distinct holiday_dt from LCBO.HOLIDAY_EXCLUSION)"
        Try
            conn.Open()
            cmd = New OracleCommand(stmt, conn)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            cmd.Dispose()
            conn.Close()
        End Try
        cmd.Dispose()
        conn.Close()
    End Sub
    'if the cut off day is a weekend day then update the status=0
    Sub UpdateWeekendEntry()
        Dim conn As OracleConnection = getConn()
        Dim cmd As OracleCommand = Nothing

        Dim stmt As String = ""
        Dim timeval As String = ""

        timeval = System.Configuration.ConfigurationManager.AppSettings("timeval")

        'stmt = "Update dbo.LCBO_CALENDAR set status=0 where to_char(cut_off_datetime,'YYYY-MM-DD') in " &
        '        " (select to_char(cut_off_datetime, 'YYYY-MM-DD') from lcbo_calendar where " &
        '        " upper(to_char(cut_off_datetime, 'DY')) in ('SAT','SUN'))"

        stmt = "Update dbo.LCBO_CALENDAR_PREVIEW set status=0 where to_char(cut_off_datetime,'YYYY-MM-DD') in " &
                " (select to_char(cut_off_datetime, 'YYYY-MM-DD') from lcbo_calendar_PREVIEW where " &
                " upper(to_char(cut_off_datetime, 'DY')) in ('SAT','SUN'))"
        Try
            conn.Open()
            cmd = New OracleCommand(stmt, conn)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            cmd.Dispose()
            conn.Close()
        End Try
        cmd.Dispose()
        conn.Close()
    End Sub


End Module
